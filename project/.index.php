<?php
namespace App;

class Project
{
    private static $livePath = [
        'smo_kogoo_scoters' => 'https://1smorgon.by/promo/kogoo/',
        'jsg_emboss_cards' => 'https://emboss.by/',
        'jsg_v1_pggame.by' => 'https://pggame.by/',
    ];

    private static $trelloID = [
//        'smokogooscoters' => 'uz2lkt6S',
//        'jsgembosscards' => 'L6zjNqS0',
    ];

    /**
     * @return array
     */
    private function getProjectArray(): array
    {
        $projectArray = [];
        $dirArray = [];
        // Opens directory
        $myDirectory = opendir(".");
        // Gets each entry
        while ($entryName = readdir($myDirectory)) {
            $dirArray[] = $entryName;
        }

        closedir($myDirectory);
        $indexCount = count($dirArray);
        for ($index = 0; $index < $indexCount; $index++) {
            $name = $dirArray[$index];
            $namehref = $dirArray[$index];
            $modtime = date("M j Y g:i A", filemtime($dirArray[$index]));
            $trelloName = str_ireplace('_', '', strtolower($name));
            if (substr("$dirArray[$index]", 0, 1) != "."
                && (is_dir($dirArray[$index]) && is_file('./.images/' . $namehref . '.jpg') || !isset($_GET['all']))) {
                $projectContent =
                    str_replace(
                        [
                            '{HREF}', '{NAME}', '{MOD_TIME}',
                            '{LIVE_HREF}', '{LIVE_DISABLED}',
                            '{TRELLO}', '{TRELLO_DISABLED}',
                        ],
                        [
                            $namehref, $name, $modtime,
                            self::$livePath[$name], self::$livePath[$name] ?? 'hidden',
                            self::$trelloID[$trelloName] . '/' . $trelloName, self::$trelloID[$trelloName] ?? 'hidden',
                        ],
                        self::getTemplate('.project.html')
                    );
                $projectArray[strtotime($modtime)] = $projectContent;

            }
        }
        krsort($projectArray);
        return $projectArray;
    }

    /**
     * @param string $fileName
     * @return string
     */
    private function getTemplate(string $fileName = ''): string
    {
        return file_get_contents($fileName);
    }

    /**
     * @return string
     */
    public static function getHTML(): string
    {
        return str_replace('{PROJECTS}', implode(self::getProjectArray(), ''), self::getTemplate('.index.html'));

    }
}

echo Project::getHTML();