# Pristrom.by - frontend

This doc provides how-to-get-started information.

## Table of Contents

1. [Pre-requirements](#1-pre-requirements)
1. [Project setup and running](#2-project-setup-and-running)
1. [Deploy](#3-deploy)
1. [Project code style](#4-project-code-style)

## 1. Pre-requirements

- node: ~14.16.0
- npm: ~6.14.11
- gulp: ~4.0.2

## 2. Project setup and running

#### 2.1 `npm` commands

- `npm install` install the dependencies in the local node_modules folder
- `npm start` build the project and start dev server on localhost:3003

#### 2.2 `gulp` commands

- `gulp build` to build the project in the `./dist` folder
- `gulp serve` to build and host it on localhost:3003

## 3. Deploy

Prepare the files through the ```npm build``` command and then use FTP to put them in the root folder to the `./dist`
folder. Redirection is carried out by the .htaccess file

## 4. Project code style

The project uses _airbnb_ codestyle [for JavaScript](https://github.com/airbnb/javascript).

## Note

In general, this is a promo site. All actual projects are placed on https://pristom.by/project
We're using Google Analytics. Use code below:

```
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-6ECMQK56RC"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-6ECMQK56RC');
</script>
```

feel free create personal google code on
https://analytics.google.com/analytics/web/#/a194226011p268481908/admin/streams/table/ 