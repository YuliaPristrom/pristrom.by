'use strict';

const {src, dest, series, watch} = require("gulp"),
    less = require("gulp-less"),
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify-es').default,
    csso = require('gulp-csso'),
    include = require('gulp-file-include'),
    htmlMin = require('gulp-htmlmin'),
    del = require('del'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    sync = require('browser-sync').create(),
    webp = require('gulp-webp'),
    webpHTML = require('gulp-webp-html'),
    webpCss = require('gulp-webpcss')

const buildParams = [clean, js, css, html, images, apache, favicon, robots]

const path = {
    build: {
        server: './dist',
        html: './dist',
        js: './dist/js',
        css: './dist/css',
        css_min: 'style.css',
        apache: './dist',
        favicon: './dist',
        robots: './dist',
        fonts: './dist/fonts',
        img: './dist/img',
        img_webp_quality: 85,
        img_optimization_level: 6,
        original_img: './dist/img/**/*.{jpg,jpeg,png,gif,ico,svg}',
        video: './dist/vid'
    },
    src: {
        html: './src/*.html',
        js: './src/js/*.js',
        less: './src/css/*.less',
        apache: './src/apache/.htaccess',
        favicon: './src/favicon.ico',
        robots: './src/robots.txt',
        fonts: './src/fonts/**',
        video: './src/vid/**',
        img: './src/img/**/*.{jpg,jpeg,png,gif,ico,svg}',
    },
    watch: {
        html: './src/**/*.html',
        js: './src/**/*.js',
        less: './src/**/*.less',
        fonts: './src/fonts/**',
        video: './src/vid/**',
        img: './src/img/**/*.{jpg,jpeg,png,gif,ico,svg}',
    }
};

function html() {
    return src(path.src.html)
        .pipe(include({
            prefix: '@@'
        }))
        .pipe(webpHTML())
        .pipe(htmlMin({
            collapseWhitespace: true,
            removeComments: true
        }))
        .pipe(dest(path.build.html))

}

function css() {
    return src(path.src.less)
        .pipe(less())
        .pipe(autoprefixer(
            {
                cascade: false
            }))
        .pipe(csso())
        .pipe(concat(path.build.css_min))
        .pipe(webpCss())
        .pipe(dest(path.build.css))
}

function images() {
    return src(path.src.img)
        .pipe(webp({
            quality: path.build.img_webp_quality
        }))
        .pipe(dest(path.build.img))
        .pipe(src(path.src.img))
        .pipe(imagemin({
            progressive: true,
            svgPlugins: [{removeViewBox: false}],
            interlaced: true,
            optimizationLevel: path.build.img_optimization_level
        }))
        .pipe(dest(path.build.img))
}

function js() {
    return src(path.src.js)
        .pipe(uglify())
        .pipe(dest(path.build.js))
}

function favicon() {
    return src(path.src.favicon)
        .pipe(dest(path.build.favicon))
}

function robots() {
    return src(path.src.robots)
        .pipe(dest(path.build.robots))
}

function apache() {
    return src(path.src.apache)
        .pipe(dest(path.build.apache))
}

function clean() {
    return del(path.build.server)
}

function serve() {
    sync.init({
        server: path.build.server
    })
    watch(path.watch.html, series(html)).on('change', sync.reload)
    watch(path.watch.less, series(css)).on('change', sync.reload)
    watch(path.watch.img, series(images)).on('change', sync.reload)
}

exports.build = series(buildParams)
exports.serve = series(buildParams, serve)
exports.clean = clean
